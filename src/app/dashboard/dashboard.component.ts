import { Component, OnInit ,ViewChild} from "@angular/core";
import { RestApiService } from "../rest-api.service";
import { AuthenticationService } from "../authentication.service";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';  

@Component({
    selector: "app-dashboard",
    templateUrl: "./dashboard.component.html",
    styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {

	propertys:any;
	emailSucess:boolean = false;
	emailError:boolean = false;

    constructor( private rest: RestApiService ,private toastr: ToastrService, private authenticationService: AuthenticationService ,private router : Router ) {
    }

    ngOnInit() {
    	this.getproperty();
        this.getcategories();
        this.homedate();
    }
    @ViewChild('emailform') form;
    getUrl() {
        return (
            "url('" + this.homedata.Header_image + "')"
        );
    }

    getCenterUrl() {
        return (
            "url('" + this.homedata.Center_image + "')"
        );
    }


    /* on page reload call the default value  */

    getproperty(){
        this.propertys = [];
        this.rest.getproperty({}).subscribe((data: any) => {
            this.propertys = data.data.data;
        });
    }
    homedata:any;

    homedate(){
        this.propertys = [];
        this.rest.homedata().subscribe((data: any) => {
            this.homedata = data.data;
        });
    }

    emailForm(form){
    	this.rest.subscribeNewsLatter(form.value).subscribe((data: any) => {
            this.form.resetForm();
            if (data.status) {
            	this.emailSucess = true;
            	this.emailError = false;
            } else {
            	this.emailError = true;
            	this.emailSucess = false;
            }
            setTimeout(()=>{
                this.emailSucess = false;
                this.emailError = false;
            }, 4000);
        });
    }


    seachproperty(form){
        localStorage.setItem('redData', JSON.stringify(form.value));
        this.router.navigate(['/find-space']);
    }

    category_id:number;
    category_name:string;
    setCate(id,name){
        (document.getElementById('cate-text') as HTMLImageElement).innerHTML = name+'<span class="caret caret_filter"></span>';
        this.category_id = id;
        this.category_name = name;
    }

    /* on page reload call get categories list   */

    categories = [];
    getcategories(){
        this.rest.getcategories().subscribe((data: any) => {
            this.categories = data.data;
        });
    }

}
