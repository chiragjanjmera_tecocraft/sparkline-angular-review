import { Component, OnInit } from "@angular/core";
import { Options, ChangeContext, PointerType } from "ng5-slider";
import { NgForm } from "@angular/forms";
import { RestApiService } from "../rest-api.service";
import { NgbDate ,NgbDateStruct, NgbCalendar ,NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AuthenticationService } from "../authentication.service";

@Component({
    selector: "app-find-space",
    templateUrl: "./find-space.component.html",
    styleUrls: ["./find-space.component.css"]
})

export class FindSpaceComponent implements OnInit {
    value: number = 0;
    logText: string = "";
    minRange: number = 0;
    maxRange: number = 5000;
    propertys:any;
    itemsCount:number;
    lat: number = 37.0560388;
    lng: number = -94.5241338;
    isFavorite:boolean;
    markers = [];
    selectedItems = [];
    location:string;
    hoveredDate: NgbDate;
    fromDate: NgbDate;
    toDate: NgbDate;
    hourlyDate: NgbDate; // hourly date
    hourlyStrTime:any;
    hourlyEndTime:any;
    options: Options = {
        floor: 0,
        ceil: 5000,
        // step: 5,
        enforceStep: false,
        enforceRange: false
    };
    group: any = {};
    houlryOrDaily:number = 0;  // if model 0 = cancel 1 = daily 2 = hourly
    islogin:boolean;
    public loading = false;
    constructor(private rest: RestApiService ,private authenticationService: AuthenticationService,calendar: NgbCalendar,config: NgbDatepickerConfig , private router : Router) {
        config.minDate = calendar.getToday();
        var mtngdate = new Date();
        var CurrentDate = new Date();
        CurrentDate.setMonth(CurrentDate.getMonth() + 6);
        var dd = CurrentDate.getDate();
        var y = CurrentDate.getFullYear();
        if (mtngdate.getMonth()+7 > 12) {
            var mnthdate = (mtngdate.getMonth()+7) - 12;
        } else {
            var mnthdate = mtngdate.getMonth();
        }
        config.maxDate = { year: y, month: mnthdate, day: dd };
    }

    ngOnInit() {
        if (localStorage.getItem('redData')) {
            var redData = JSON.parse(localStorage.getItem('redData'));
            if (redData.location) {
                this.location = redData.location;
            }
            if (redData.category_id) {
                this.category_id = redData.category_id;  
            }
        }
        this.getproperty();
        this.getamenities();
        this.getcategories();
        var data = JSON.parse(localStorage.getItem('currentUser'));
        if (data) {
            this.islogin = true;
        }
    }

    /* on page reload call the default value  */

    getproperty(){
        this.propertys = [];
        this.loading = true;
        this.rest.getproperty({location:this.location ,category_id : this.category_id}).subscribe((data: any) => {
            this.loading = false;
            this.propertys = data.data.data;
            this.itemsCount = data.data.total;
            this.setMaker();
        });
        var redData = JSON.parse(localStorage.getItem('redData'));
        if (redData.category_name) {
            (document.getElementById('category-id') as HTMLImageElement).innerHTML = redData.category_name+'<span class="caret caret_filter"></span>';
        }
        localStorage.setItem('redData', JSON.stringify({}));
    }

    /* on page reload call get amenities list   */

    amenities = [];
    dropdownList = [];
    dropdownSettings = {};
    getamenities(){
        this.rest.getamenities().subscribe((data: any) => {
            this.amenities = data.data;
            var amenitiesarray = [];
            for (var i= 0;i<this.amenities.length;i=i+1)
            {
                amenitiesarray.push({item_id : this.amenities[i].id,item_text : this.amenities[i].name})
            }
            this.dropdownList = amenitiesarray;
            this.dropdownSettings = {
                singleSelection: false,
                idField: 'item_id',
                textField: 'item_text',
                selectAllText: 'Select All',
                unSelectAllText: 'UnSelect All',
                itemsShowLimit: 3,
                allowSearchFilter: true
            };
        });
    }

    /* on page reload call get categories list   */

    categories = [];
    getcategories(){
        this.rest.getcategories().subscribe((data: any) => {
            this.categories = data.data;
        });
    }

    /* set marker  */

    // /* on event change fetch property details */

    setMaker(){
        this.markers = [];
        if (typeof this.propertys !== 'undefined' && this.propertys.length > 0 ) {
            for (var i = 0; i < this.propertys.length; i++) {
                let imgpath = (this.propertys[i].property_images[0]);
                if (typeof imgpath == 'undefined') {
                    imgpath = {
                        image : '/public/images/property/space-8/1563170104.jpg'
                    }
                }
                this.markers.push({lat:this.propertys[i].latitude,lng:this.propertys[i].longitude,isOpen :false,
                    draggable: false,street:this.propertys[i].street,city:this.propertys[i].city,
                    maximum_total:this.propertys[i].maximum_total,id:i,img:imgpath,name:this.propertys[i].name,
                    daily_rate:this.propertys[i].daily_rate});
            }
        }
    }


    getOnChangeProperty(pageNumber,form){
        if (pageNumber) {
            form.page = pageNumber.pageNo;
        }
        this.propertys = [];
        this.loading = true;
        this.rest.getOnChangeProperty(form).subscribe((data: any) => {
            if (data) {
                this.loading = false;                
                this.propertys = data.data.data;
                this.itemsCount = data.data.total;
                this.setMaker();
            }
        });
    }

    onchange(form: NgForm = null) {
        this.priceType //  1 = low - high 2 = high - low
        this.numPeople // 1-4 5-9 10-14 15-19 20-29 30+
        this.category_id //  1 2 3 4 5 6 7 8 9 // any value of this
        let amenitiesarray = [];
        this.selectedItems.forEach(function (value) {
            amenitiesarray.push(value.item_id);
        }); 
        let amenitiesstr = amenitiesarray.toString(); // converted array to string
        let price = this.minRange + '-' + this.maxRange;
        var formdata:any = {
            location : this.location,
            amenities: amenitiesstr,
            category_id : this.category_id,
            attendance : this.numPeople,
            sort_by_price : this.priceType,
            page: this.page,
            price : this.minRange+'-'+this.maxRange,
        }
        if (this.houlryOrDaily == 1) { // 1 = daily
            formdata.type = 1; 
            formdata.date = this.setdateformat(this.fromDate);
            formdata.end_date = this.setdateformat(this.toDate);
        } else if(this.houlryOrDaily == 2){ // 2 = hourly
            formdata.date = this.setdateformat(this.hourlyDate);
            formdata.type = 0; 
            formdata.start_time = Number(this.hourlyEndTime) - Number(this.hourlyStrTime);
        }
        this.getOnChangeProperty('',formdata);
    }

    keyDownFunction(event) {
        if (event.keyCode == 13) {
            alert("you just clicked enter");
            // rest of your code
        }
    }
    page:number;
    pageChanged(event,signupForm){
        if (event.event.itemsCount) { 
        } else {
            this.page = event.pageNo;
            this.onchange();
        }
    }

    open(id) {
        this.markers.forEach(function (element, i) {
            element.isOpen = false;
            if (i == id) {
                element.isOpen = true;
            }
        });
    }

    close() {
        this.markers.forEach(function (element, i) {
            element.isOpen = false;
        });
    }

    priceType:number;
    setType(name,value){
        (document.getElementById('price-short-id') as HTMLImageElement).innerHTML = name+'<span class="caret caret_filter"></span>';
        this.priceType = value;
        this.onchange();
    }

    numPeople:string;
    setPeople(value){
        (document.getElementById('attendeed-id') as HTMLImageElement).innerHTML = value+'<span class="caret caret_filter"></span>';
        value = value == 'any' ? '' : value == '30+' ? '30' : value;
        this.numPeople = value;
        this.onchange();
    }

    /* set category */
    category_id:number;
    setCate(id,name){
        (document.getElementById('category-id') as HTMLImageElement).innerHTML = name+'<span class="caret caret_filter"></span>';
        this.category_id = id;  
        this.onchange();
    }

    onDateSelection(date: NgbDate) {
        if (!this.fromDate && !this.toDate) {
          this.fromDate = date;
        } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
          this.toDate = date;
        } else {
          this.toDate = null;
          this.fromDate = date;
        }
    }

    isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }

    isInside(date: NgbDate) {
        return date.after(this.fromDate) && date.before(this.toDate);
    }

    isRange(date: NgbDate) {
        return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
    }
    
    hourlyStartTime = ['6.00', '6.30', '7.00', '7.30', '8.00', '8.30', '9.00', '9.30', '10.00', '10.30', '11.00',
        '11.30', '12.00', '12.30', '13.00', '13.30', '14.00', '14.30', '15.00', '15.30',
        '16.00', '16.30', '17.00', '17.30', '18.00',
        '18.30', '19.00', '19.30', '20.00', '20.30', '21.00', '21.30', '22.00', '22.30'];

    new_arr = [];
    hourlyStrtm:any; 
    hourlyEndtm:any; 
    setFromtime(time) {
        time = Number(time) + 1.5;
        var new_arry = this.hourlyStartTime.filter(function (x) {
            return Number(x) > (Number(time));
        });
        this.new_arr = new_arry;
    }
    
    /* daily and hourly modal apply button click */

    setDailyType(type){
        this.houlryOrDaily = type;
        if (type == 1) {
            strt = '';
            if (this.fromDate) {
                var strt = this.setdateformat(this.fromDate);
            }
            enddate = '';
            if (this.toDate) {
                var enddate = '-' + this.setdateformat(this.toDate);
            }
            (document.getElementById('datePlcHld') as HTMLInputElement).placeholder = strt+''+enddate;
        } else {
            let strdate = this.setdateformat(this.hourlyDate);
            let eTime = this.hourlyStrTime+'-'+this.hourlyEndTime;
            (document.getElementById('datePlcHld') as HTMLInputElement).placeholder = strdate +' '+ eTime;
        }
        this.onchange();
    }

    setDBlank(){
        this.houlryOrDaily = 0;
        (document.getElementById('datePlcHld') as HTMLInputElement).placeholder = 'Date';
    }

    setdateformat(event) {
        if (String(event.day).length == 1) {
            var day = '0' + event.day;
        } else {
            var day = '' + event.day;
        }
        if (String(event.month).length == 1) {
            var month = '0' + event.month
        } else {
            var month = '' + event.month
        }
        let fulldate = day + '/' + month + '/' + event.year;
        return fulldate;
    }

    onEvent(event,propery_id) {
        event.stopPropagation();
        this.rest.togglelike({property_id:propery_id}).subscribe((data: any) => {
        });
    }

    notLoginUser(event){
        event.stopPropagation();
        this.authenticationService.IsopenModel("true");
    }

    /*login and sign up modal*/

    reloadpage(){
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
        this.router.navigate(["/find-space"]));
    }

}
