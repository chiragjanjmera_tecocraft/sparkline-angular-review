import {
    Component,
    OnInit
} from '@angular/core';
import {
    RestApiService
} from "../../rest-api.service";

@Component({
    selector: 'app-site-footer',
    templateUrl: './site-footer.component.html',
    styleUrls: ['./site-footer.component.css']
})
export class SiteFooterComponent implements OnInit {

    config: any = [];
    mobile_number: number;
    email: string;
    address: string;
    youtube: string
    Facebook: string
    Twitter: string
    Googleplus: string
    Linkedin: string;
    proprtydata:any;
    property1:any;
    property2:any;
    image1:any;
    image2:any;

    constructor(private rest: RestApiService) {}

    ngOnInit() {
        this.config = [];
        this.rest.getconfiguration().subscribe((data: {}) => {
            this.config = data;
            this.mobile_number = this.config.data[2].value;
            this.email = this.config.data[3].value;
            this.address = this.config.data[8].value;
            this.youtube = this.config.data[9].value;
            this.Facebook = this.config.data[10].value;
            this.Twitter = this.config.data[11].value;
            this.Googleplus = this.config.data[12].value;
            this.Linkedin = this.config.data[13].value;
        });
        this.rest.getproperty({}).subscribe((data) => {
            this.proprtydata = data;
            this.property1   = this.proprtydata.data.data[0];
            this.property2   = this.proprtydata.data.data[1];
            if ( typeof (this.property1.property_images[0]) !== "undefined") {
                this.image1      = this.property1.property_images[0].image;
            }
            if (this.property2.property_images[1]) {
                this.image2      = this.property2.property_images[0].image;
            }
        });
        this.getcategories();
    }

    /* on page reload call get categories list   */

    categories = [];
    getcategories(){
        this.rest.getcategories().subscribe((data: any) => {
            this.categories = data.data;
        });
    }

}