
import { Component, OnInit ,ViewChild ,NgZone  } from "@angular/core";
import { AuthenticationService } from "../../authentication.service";
import { Router } from "@angular/router";
import { RestApiService } from "../../rest-api.service";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';  
@Component({
    selector: "app-site-header",
    templateUrl: "./site-header.component.html",
    styleUrls: ["./site-header.component.css"]
})

export class SiteHeaderComponent implements OnInit {

    userName: string;
    data                 = {};
    newclass: number     = 1;
    loginSuccess:boolean = false;
    loginFailed:boolean  = false;
    islogin:boolean      = false;
    regiSuccess:boolean  = false;
    regiFailed:boolean   = false;
    IsopenModel:boolean  = false;
    public loading       = false;
    forpassmodel:boolean = false;
    ifEmalnt:boolean = false;
    @ViewChild('userlogin') form;
    @ViewChild('userRegitration') userRegitration;
    @ViewChild('fasfasfas') forgotpassmdl;

    constructor(private authenticationService: AuthenticationService,private router: Router ,
          private zone: NgZone,private rest: RestApiService,private toastr: ToastrService) {
        this.authenticationService.myMethod$.subscribe(data => {
            this.newclass = data == "/" ? 1 : 2;
        });

        this.authenticationService.IsopenModel$.subscribe(data => {
            this.isModalShown = true;
        });
    }

    ngOnInit() {
        this.newclass = this.router.url == "/" ? 1 : 2;
        if (this.rest.currentUser()) {
            this.islogin = true;
        }
    }

    userLogin(form){        
        form.access_type = 1 ; //1 for web, 2 -for app
        this.rest.userLogin(form.value).subscribe((data: any) => {
            this.form.resetForm();
            if (data.status) {
                let element: HTMLElement = document.getElementsByClassName('closemodelbtn')[0] as HTMLElement;
                element.click();
                this.islogin = true;
                this.removeMsg();
                this.loginSuccess = true;
                this.onHidden();
                this.router.navigate(['/myprofile']);
            } else {
                this.removeMsg();
                this.loginFailed = true;
            }
        });
    }

    logout(){
        this.rest.logout();
        this.islogin = false;
        this.zone.runOutsideAngular(() => {
            location.reload();
        });
    }

    userRegistration(form){
        this.loading = true;
        this.rest.userRegistration(form.value).subscribe((data: any) => {
            this.loading = false;
            this.userRegitration.resetForm();
            if (data.status) {
                let logintestdemoelement: HTMLElement = document.getElementById('logintestdemo') as HTMLElement;
                logintestdemoelement.click();
                let tab1element: HTMLElement = document.getElementById("tab1") as HTMLElement;
                tab1element.classList.add("active");
                let tab2element: HTMLElement = document.getElementById("tab2") as HTMLElement;
                tab2element.classList.remove("active");
                this.removeMsg();
                this.regiSuccess = true;
            } else {
                this.removeMsg();
                let tab2element: HTMLElement = document.getElementById("tab2") as HTMLElement;
                tab2element.classList.remove("active");
                this.regiFailed = true;
            }
        });
    }

    /*  remove message on click on tag */

    removeMsg(){
        this.regiSuccess = false;
        this.regiFailed = false;
        this.loginFailed = false;
        this.loginSuccess = false;
    }

    resetFrm(){
        this.form.resetForm();
        this.userRegitration.resetForm();
    }

    /* on sign up link active class add */

    addmodelClass(){
        let tab2newelement: HTMLElement = document.getElementById("tab2") as HTMLElement;
        tab2newelement.classList.add("active");
        let tab1newelement: HTMLElement = document.getElementById("tab1") as HTMLElement;
        tab1newelement.classList.remove("active");
    }

    forgetPass(form){
        this.ifEmalnt = false;
        this.rest.forgetPassword(form.value).subscribe((data: any) => {
            this.forgotpassmdl.resetForm();
            if (data.status) {
                this.toastr.success(data.message, "Success!");
                this.newonHidden();
                this.showModal();
            } else {
                this.ifEmalnt = true;
                setTimeout(()=>{
                    this.ifEmalnt = false;
                }, 3000);
            }
        });
    }

    isModalShown = false;


    showModal(): void {
        this.removeMsg();
        this.isModalShown = true;
    }

    newShowModel():void{
        this.isModalShown = true;   
    }

    newonHidden(){
        this.forpassmodel = false;
    }

    onHidden(): void {
        this.isModalShown = false;
        this.removeMsg();
    }

}
