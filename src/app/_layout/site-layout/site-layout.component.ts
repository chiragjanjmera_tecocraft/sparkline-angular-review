import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService } from "../../authentication.service";

@Component({
    selector: "app-site-layout",
    templateUrl: "./site-layout.component.html",
    styleUrls: ["./site-layout.component.css"]
})
export class SiteLayoutComponent implements OnInit {
    NotDashPage: boolean = true;
    highlightedDiv: number;
    data = {};
    constructor(private router: Router, private authenticationService: AuthenticationService) {}

    ngOnInit() {
    }

    changeOfRoutes() {
        this.authenticationService.myMethod(this.router.url);
        window.scrollTo(0, 0); // Scroll to top on Route Change
    }
}
