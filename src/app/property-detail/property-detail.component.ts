import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../authentication.service";
import { RestApiService } from "../rest-api.service";
import { Routes, RouterModule, ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions } from "ngx-gallery";
import { NgbDate ,NgbDateStruct, NgbCalendar ,NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: "app-property-detail",
    templateUrl: "./property-detail.component.html",
    styleUrls: ["./property-detail.component.css"]
})
export class PropertyDetailComponent implements OnInit {
    property: any;
    rating = [];
    image = "";
    galleryOption: NgxGalleryOptions[];
    galleryImage: NgxGalleryImage[];
    propertyImgs = [];
    lat: number ;
    lng: number ;
    hoveredDate: NgbDate;
    fromDate: NgbDate;
    toDate: NgbDate;
    hourlyDate: any; // hourly date
    hourlyStrTime:any;
    hourlyEndTime:any;
    markDisabled:any;
    getdate:any;
    diasbledatearray:any;
    diasbledatearray1:any;
    markDisabled1:any;
    showcount:boolean;
    configArray:any;
    vatPercentage:any;
    Settype:number = 0; // 1 = hourly 2 = daily //

    constructor( private authenticationService: AuthenticationService, private rest: RestApiService, private route: ActivatedRoute, private router: Router
                ,calendar: NgbCalendar,config: NgbDatepickerConfig) {
        config.minDate = calendar.getToday();
        var mtngdate = new Date();
        var CurrentDate = new Date();
        CurrentDate.setMonth(CurrentDate.getMonth() + 6);
        var dd = CurrentDate.getDate();
        var y = CurrentDate.getFullYear();
        if (mtngdate.getMonth()+7 > 12) {
            var mnthdate = (mtngdate.getMonth()+7) - 12;
        } else {
            var mnthdate = mtngdate.getMonth();
        }
        config.maxDate = { year: y, month: mnthdate, day: dd };
        if (calendar.getToday().day.toString().length == 1) {
            var day = '0' + calendar.getToday().day;
        } else {
            var day = '' + calendar.getToday().day;
        }
        if (calendar.getToday().month.toString().length == 1) {
            var month = '0' + calendar.getToday().month
        } else {
            var month = '' + calendar.getToday().month
        }
        let date1 = day + '/' + month + '/' + calendar.getToday().year;
        let date2 = day + '/' + month + '/' + calendar.getToday().year;
        this.route.params.subscribe(params => {
            let id = +params["id"]; 
            this.rest.getdiabledate(date1, id,1).subscribe((data) => {
                this.diasbledatearray = data;
                this.updateDisabledDates(this.diasbledatearray); // ,0 Hourly, 1 Daily
            });
            this.rest.getdiabledate(date2, id,0).subscribe((data) => {
                this.diasbledatearray1 = data;
                this.updateDisabledDates1(this.diasbledatearray1); // 0 Hourly, 1 Daily
            });
        });
        this.rest.getconfiguration().subscribe((data: any) => {
            this.configArray = data.data;
            for (let idx = 0; idx < this.configArray.length; idx++) {
                if (this.configArray[idx].name == "Vat_Percentage") {
                    this.vatPercentage = this.configArray[idx].value;
                }
            }
        });
    }
    property_id:number;
    ngOnInit() {
        this.route.params.subscribe(params => {
            let id = +params["id"]; // (+) converts string 'id' to a number
            this.property_id = id;
            var prop_id = {
                property_id: id
            };
            this.rest.getOnChangeProperty(prop_id).subscribe((data: any) => {
                if (data.status) {
                    this.property = data.data.data[0];
                    this.lat = Number(this.property.latitude);
                    this.lng = Number(this.property.longitude);
                    if (this.property.property_images[0]) {
                        this.image = this.property.property_images[0].image;
                        this.galleryOption = [
                            {
                                imageAutoPlay: true,
                                imageAutoPlayPauseOnHover: true,
                                previewAutoPlay: true,
                                previewAutoPlayPauseOnHover: true
                            },
                            {
                                imageArrowsAutoHide: true,
                                thumbnailsArrowsAutoHide: true
                            },
                            {
                                width: "100%",
                                height: "500px",
                                thumbnailsColumns: 4,
                                imageAnimation: NgxGalleryAnimation.Slide
                            },
                            {
                                breakpoint: 800,
                                width: "100%",
                                height: "600px",
                                imagePercent: 80,
                                thumbnailsPercent: 20,
                                thumbnailsMargin: 20,
                                thumbnailMargin: 20
                            },
                            {
                                breakpoint: 400,
                                preview: false
                            }
                        ];

                        this.property.property_images.forEach(data => {
                            this.propertyImgs.push({
                                small: 'https://sparklinespace.tecocraft.co.in/console' + data.image,
                                medium: 'https://sparklinespace.tecocraft.co.in/console' + data.image,
                                big: 'https://sparklinespace.tecocraft.co.in/console' + data.image
                            });
                        });

                        this.galleryImage = this.propertyImgs;
                    }
                    let ratingVal = Math.round(data.data.data[0].avg_ratings);
                    for (var i = 0; i < ratingVal; ++i) {
                        this.rating.push(i);
                    }
                } else {
                    this.router.navigate(["/"]);
                }
            });
        });
    }
    isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }

    isInside(date: NgbDate) {
        return date.after(this.fromDate) && date.before(this.toDate);
    }

    isRange(date: NgbDate) {
        return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
    }

    getUrl() {
        return (
            "url('https://sparklinespace.tecocraft.co.in/console/" + this.image + "')"
        );
    }

    updateDisabledDates(diasbledatearray) {
        var cuurentdate = new Date();
        var currentMoAarry = new Array();
        var currentYeAarry = new Array();
        var currentmonth = cuurentdate.getMonth();
        currentMoAarry.push(currentmonth + 1);
        currentYeAarry.push(cuurentdate.getFullYear());
        var firstdate = new Array();
        var seconddate = new Array();
        var thirddate = new Array();
        var fourthdate = new Array();
        var fivedate = new Array();
        var sixdate = new Array();
        // months
        var firstmonth = new Array();
        var secondmonth = new Array();
        var thirdmonth = new Array();
        var fourthmonth = new Array();
        var fivemonth = new Array();
        var sixmonth = new Array();
        // year
        var firstyear = new Array();
        var secondyear = new Array();
        var thirdyear = new Array();
        var fourthyear = new Array();
        var fiveyear = new Array();
        var sixyear = new Array();
        for (var item of diasbledatearray.data.dates) {
            var datearray = (item.toString()).split('/');
            var formateddate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
            const d = new Date(formateddate);
            if (d.getMonth() == currentmonth) {
                firstdate.push(d.getDate());
                firstmonth.push(d.getMonth() + 1);
                firstyear.push(d.getFullYear());
            } else if (d.getMonth() == (currentmonth + 1)) {
                seconddate.push(d.getDate());
                (d.getMonth() + 1) > 12 ? secondmonth.push((d.getMonth() + 1) - 12) : secondmonth.push(d.getMonth() + 1);
                (d.getMonth() + 1) > 12 ? secondyear.push(d.getFullYear() + 1) : secondyear.push(d.getFullYear());
            } else if (d.getMonth() == (currentmonth + 2)) {
                thirddate.push(d.getDate());
                (d.getMonth() + 2) > 12 ? thirdmonth.push((d.getMonth() + 1) - 12) : thirdmonth.push(d.getMonth() + 1);
                (d.getMonth() + 2) > 12 ? thirdyear.push(d.getFullYear() + 1) : thirdyear.push(d.getFullYear());
            } else if (d.getMonth() == currentmonth + 3) {
                fourthdate.push(d.getDate());
                (d.getMonth() + 3) > 12 ? fourthmonth.push((d.getMonth() + 1) - 12) : fourthmonth.push(d.getMonth() + 1);
                (d.getMonth() + 3) > 12 ? fourthyear.push(d.getFullYear() + 1) : fourthyear.push(d.getFullYear());
            } else if (d.getMonth() == currentmonth + 4) {
                fivedate.push(d.getDate());
                (d.getMonth() + 4) > 12 ? fiveyear.push((d.getMonth() + 1) - 12) : fiveyear.push(d.getMonth() + 1);
                (d.getMonth() + 4) > 12 ? fiveyear.push(d.getFullYear() + 1) : fiveyear.push(d.getFullYear());
            } else if (d.getMonth() == currentmonth + 5) {
                sixdate.push(d.getDate());
                (d.getMonth() + 5) > 12 ? sixyear.push((d.getMonth() + 1) - 12) : sixyear.push(d.getMonth() + 1);
                (d.getMonth() + 5) > 12 ? sixyear.push(d.getFullYear() + 1) : sixyear.push(d.getFullYear());
            }
        }
        var text = [];
        var i;
        const getdatecheck = new Date();
        if (((getdatecheck.getDate()).toString()).length == 1) {
            var day = '0' + getdatecheck.getDate();
        } else {
            var day = '' + getdatecheck.getDate();
        }
        if (((getdatecheck.getMonth() + 1).toString()).length == 1) {
            var month = '0' + (getdatecheck.getMonth() + 1);
        } else {
            var month = '' + (getdatecheck.getMonth() + 1);
        }
        if (((getdatecheck.getHours()).toString()).length == 1) {
            var hourly = '0' + getdatecheck.getHours();
        } else {
            var hourly = '' + getdatecheck.getHours();
        }
        if (((getdatecheck.getMinutes() + 1).toString()).length == 1) {
            var minite = '0' + (getdatecheck.getMinutes() + 1);
        } else {
            var minite = '' + (getdatecheck.getMinutes() + 1);
        }
        let date = day + '/' + month + '/' + getdatecheck.getFullYear();
        this.markDisabled = (date: NgbDate) => {
            const d = new Date(date.year, date.month - 1, date.day);
            var arryschedule = diasbledatearray.data.schedule;
            if (arryschedule[0]['is_available'] == 0) {
                var weekno0 = d.getDay() == 0;
            } else {
                var weekno0 = d.getDay() == -1;
            }
            if (arryschedule[1]['is_available'] == 0) {
                var weekno1 = d.getDay() == 1;
            } else {
                var weekno1 = d.getDay() == -1;
            }
            if (arryschedule[2]['is_available'] == 0) {
                var weekno2 = d.getDay() == 2;
            } else {
                var weekno2 = d.getDay() == -1;
            }
            if (arryschedule[3]['is_available'] == 0) {
                var weekno3 = d.getDay() == 3;
            } else {
                var weekno3 = d.getDay() == -1;
            }
            if (arryschedule[4]['is_available'] == 0) {
                var weekno4 = d.getDay() == 4;
            } else {
                var weekno4 = d.getDay() == -1;
            }
            if (arryschedule[5]['is_available'] == 0) {
                var weekno5 = d.getDay() == 5;
            } else {
                var weekno5 = d.getDay() == -1;
            }
            if (arryschedule[6]['is_available'] == 0) {
                var weekno6 = d.getDay() == 6;
            } else {
                var weekno6 = d.getDay() == -1;
            }

            return weekno0 || weekno1 || weekno2 || weekno3 || weekno4 || weekno5 || weekno6 || (firstdate.indexOf(date.day) >= 0 && (firstmonth).indexOf(date.month) >= 0 && (firstyear).indexOf(date.year) >= 0) ||
                (seconddate.indexOf(date.day) >= 0 && secondmonth.indexOf(date.month) >= 0 && secondyear.indexOf(date.year) >= 0);
            (thirddate.indexOf(date.day) >= 0 && (thirdmonth).indexOf(date.month) >= 0 && (thirdyear).indexOf(date.year) >= 0) ||
            (fourthdate.indexOf(date.day) >= 0 && (fourthmonth).indexOf(date.month) >= 0 && (fourthyear).indexOf(date.year) >= 0) ||
            (fivedate.indexOf(date.day) >= 0 && (fivemonth).indexOf(date.month) >= 0 && (fiveyear).indexOf(date.year) >= 0) ||
            (sixdate.indexOf(date.day) >= 0 && (sixmonth).indexOf(date.month) >= 0 && (sixyear).indexOf(date.year) >= 0);
        }
    }
   
    updateCalcs(event) {
        const getdatecheck = new Date();
        var demo = 10;
        if (getdatecheck.getDate() == event.day && (getdatecheck.getMonth() + 1) == event.month && getdatecheck.getFullYear() == event.year) {
            if (((getdatecheck.getDate()).toString()).length == 1) {
                var day = '0' + getdatecheck.getDate();
            } else {
                var day = '' + getdatecheck.getDate();
            }
            if (((getdatecheck.getMonth() + 1).toString()).length == 1) {
                var month = '0' + (getdatecheck.getMonth() + 1);
            } else {
                var month = '' + (getdatecheck.getMonth() + 1);
            }
            if (((getdatecheck.getHours()).toString()).length == 1) {
                var hourly = '0' + getdatecheck.getHours();
            } else {
                var hourly = '' + getdatecheck.getHours();
            }
            if (((getdatecheck.getMinutes() + 1).toString()).length == 1) {
                var minite = '0' + (getdatecheck.getMinutes() + 1);
            } else {
                var minite = '' + (getdatecheck.getMinutes() + 1);
            }
            let date = day + '/' + month + '/' + getdatecheck.getFullYear();
            this.hourlyDate = date;
            this.rest.getdate(date, this.property_id, hourly + ':' + minite).subscribe((data) => {
                this.getdate = data;
            });
        } else {
            if (((event.day).toString()).length == 1) {
                var day = '0' + event.day;
            } else {
                var day = '' + event.day;
            }
            if (((event.month).toString()).length == 1) {
                var month = '0' + event.month
            } else {
                var month = '' + event.month
            }
            let date = day + '/' + month + '/' + event.year;
            this.hourlyDate = date;
            this.rest.getdate(date, this.property_id, '').subscribe((data) => {
                this.getdate = data.data;
            });
        }
    }

    totime: any;
    settime(time) {
        if (time == '--') {
            this.totime = [];
        } else {
            var array = (time.toString()).split(':');
            var convertedtime = (Number(array[0]) + 1.5) + ':' + array[1];
            var hrly = (this.hourlyDate).split('/');
            if (hrly[0].length == 1) {
                var day = '0' + hrly[0];
            } else {
                var day = '' + hrly[0];
            }
            if (hrly[1].length == 1) {
                var month = '0' + hrly[1]
            } else {
                var month = '' + hrly[1]
            }
            let date = day + '/' + month + '/' + hrly[2];
            this.rest.getdate(date, this.property_id, convertedtime).subscribe((data) => {
                this.totime = data.data;
            });
        }
    }

    percenatgevalue: any;
    percentage(partialValue, totalValue) {

        var percenatgevalue = (partialValue / 100) * totalValue;
        this.percenatgevalue = percenatgevalue.toFixed(2);
        var finalvalue = +totalValue + (+percenatgevalue);
        return finalvalue.toFixed(2);
    }

    countprice: string;
    totalhourseprice: number;
    errorclass:boolean;
    withVat:any;
    alreadyDateError:boolean =false;
    onDateSelection(date: NgbDate) {
        this.osshowerror = false;
        this.selecDateError = false;
        this.loginDateError = false;
        this.alreadyDateError = false;
        if (!this.fromDate && !this.toDate) {
            this.Settype = 2 ; 
            this.fromDate = date;
            this.toDate = null;
            this.errorclass = false;
            this.showcount = true;
            this.countprice = '$' + this.property.daily_rate + ' x 1 day'; // count price
            this.totalhourseprice = this.property.daily_rate; // hourly price      this.errorclass = false;
            this.withVat = this.percentage(this.vatPercentage, this.totalhourseprice); // hourly price      this.errorclass = false;
            let datefrm = this.fromDate.day + '/' + this.fromDate.month + '/' + this.fromDate.year;
            let tsnString = (document.getElementById("check_in") as HTMLTextAreaElement);
            tsnString.placeholder = datefrm;
        } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
            this.toDate = date;
            let frmdat = this.fromDate.year + ',' + this.fromDate.month + ',' + this.fromDate.day;
            let todat = this.toDate.year + ',' + this.toDate.month + ',' + this.toDate.day;
            if (((this.fromDate.day).toString()).length == 1) {
                var day = '0' + this.fromDate.day;
            } else {
                var day = '' + this.fromDate.day;
            }
            if (((this.fromDate.month).toString()).length == 1) {
                var month = '0' + this.fromDate.month;
            } else {
                var month = '' + this.fromDate.month;
            }
            if (((this.toDate.day).toString()).length == 1) {
                var tday = '0' + this.toDate.day;
            } else {
                var tday = '' + this.toDate.day;
            }
            if (((this.toDate.month).toString()).length == 1) {
                var tomonth = '0' + this.toDate.month;
            } else {
                var tomonth = '' + this.toDate.month;
            }
            let datefrm = day + '/' + month + '/' + this.fromDate.year;
            let dateto = tday + '/' + tomonth + '/' + this.toDate.year;
            let tsnString = (document.getElementById("check_in") as HTMLTextAreaElement);
            tsnString.placeholder = datefrm + ' - ' + dateto;
            let type = 1;
            this.rest.checkavalablity(this.property_id, datefrm, dateto, type).subscribe((response: any) => {
                if (response.data.is_available > 0) {
                    this.Settype = 2 ; 
                    this.showcount = true;
                    this.errorclass = false;
                    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                    var firstDate = new Date(frmdat);
                    var secondDate = new Date(todat);
                    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                    this.countprice = '$' + this.property.daily_rate + ' x ' + (diffDays + 1) + ' days'; // count price
                    this.totalhourseprice = this.property.daily_rate * (diffDays + 1); // hourly price
                    this.withVat = this.percentage(this.vatPercentage, this.totalhourseprice); // hourly price
                } else {
                    this.Settype = 0 ; 
                    this.alreadyDateError = true;
                    this.osshowerror = false;
                    this.selecDateError = false;
                    this.loginDateError = false;
                }
            });
        } else {
            this.Settype = 2 ; 
            this.toDate = null;
            this.errorclass = false;
            this.fromDate = date;
            this.showcount = true;
            this.countprice = '$' + this.property.daily_rate + ' x 1 day'; // count price
            this.totalhourseprice = this.property.daily_rate; // hourly price
            this.withVat = this.percentage(this.vatPercentage, this.totalhourseprice);; // hourly price
            let datefrm = this.fromDate.day + '/' + this.fromDate.month + '/' + this.fromDate.year;
            let tsnString = (document.getElementById("check_in") as HTMLTextAreaElement);
            tsnString.placeholder = datefrm;
        }
    }

    setTotime(time) {
        this.osshowerror = false;
        this.selecDateError = false;
        this.loginDateError = false;
        this.alreadyDateError = false;
        if (time !== '--') {
            let tsnString = (document.getElementById("check_in") as HTMLTextAreaElement);
            tsnString.placeholder = this.hourlyDate + '   ' + this.hourlyStrTime + '-' + time;
            this.showcount = true;
            this.errorclass = false;
            var start = ((this.hourlyStrTime).toString()).split(":");
            var end = (time.toString()).split(":");
            var startDate = new Date(0, 0, 0, Number(start[0]), Number(start[1]), 0);
            var endDate = new Date(0, 0, 0, end[0], end[1], 0);
            var diff = endDate.getTime() - startDate.getTime();
            var hours = Math.floor(diff / 1000 / 60 / 60);
            diff -= hours * 1000 * 60 * 60;
            var minutes = Math.floor(diff / 1000 / 60);
            var hoursdiff = hours + ":" + (minutes < 9 ? "0" : "") + minutes;
            this.countprice = '$' + this.property.hourly_rate + ' * ' + hoursdiff; // count price
            var hourarray = (hoursdiff.toString()).split(':');
            if (hourarray[1] == '15') {
                var finaltime = Number(hourarray[0]) * 1.25;
            } else if (hourarray[1] == '30') {
                var finaltime = Number(hourarray[0]) * 1.50;
            } else if (hourarray[1] == '45') {
                var finaltime = Number(hourarray[0]) * 1.75;
            } else if (hourarray[1] == '60') {
                var finaltime = Number(hourarray[0]) * 1;
            } else if (hourarray[1] == '00') {
                var finaltime = Number(hourarray[0]) * 1;
            }
            this.totalhourseprice = this.property.hourly_rate * Number(finaltime); // hourly price
            this.withVat = this.percentage(this.vatPercentage, this.totalhourseprice);
            this.Settype = 1 ; 
        }
    }

    openmodel(){
        this.authenticationService.IsopenModel("demo");
    }
    osshowerror:boolean = false;
    selecDateError:boolean = false;
    loginDateError:boolean = false;
    booknow() {
        if (this.showcount) { // showcount means date not selected
            if (this.errorclass) {
                this.osshowerror = true;
                this.selecDateError = false;
                this.loginDateError = false;
                this.alreadyDateError = false;
            } else {
                if (this.Settype !== 0) {
                    if (this.rest.currentUser() !== null && this.rest.currentUser().id !== undefined) {
                    let hourlyDataT:any = {};
                    let dailyDataT:any = {};
                    if (this.Settype == 1) { // hourly
                        hourlyDataT.date = this.hourlyDate;
                        hourlyDataT.strTime = this.hourlyStrTime;
                        hourlyDataT.endTime = this.hourlyEndTime;
                        hourlyDataT.property_id = this.property_id;
                        hourlyDataT.price = this.countprice;
                        hourlyDataT.vatPercentage = this.vatPercentage;
                        hourlyDataT.percenatgevalue = this.percenatgevalue;
                        hourlyDataT.type = 0;
                        hourlyDataT.withVat = this.withVat;
                    } else if(this.Settype == 2){
                        dailyDataT.strdate =  this.fromDate;
                        dailyDataT.enddate = this.toDate;
                        dailyDataT.property_id = this.property_id;
                        dailyDataT.price = this.countprice;
                        dailyDataT.vatPercentage = this.vatPercentage;
                        dailyDataT.percenatgevalue = this.percenatgevalue;
                        dailyDataT.type = 1;
                        dailyDataT.withVat = this.withVat;
                    }
                    let arytoTran = [hourlyDataT,dailyDataT];
                    this.rest.setbookingData(arytoTran);
                    this.router.navigate(['/booking']);
                } else {
                    this.osshowerror = false;
                    this.selecDateError = false;
                    this.loginDateError = true;
                    this.alreadyDateError = false;
                    this.authenticationService.IsopenModel("true");
                }
                }
            }
        } else {
            this.osshowerror = false;
            this.selecDateError = true;
            this.loginDateError = false;
            this.alreadyDateError = false;
        }
    }
     updateDisabledDates1(diasbledatearray) {
        var cuurentdate = new Date();
        var currentMoAarry = new Array();
        var currentYeAarry = new Array();
        var currentmonth = cuurentdate.getMonth();
        currentMoAarry.push(currentmonth + 1);
        currentYeAarry.push(cuurentdate.getFullYear());
        var firstdate = new Array();
        var seconddate = new Array();
        var thirddate = new Array();
        var fourthdate = new Array();
        var fivedate = new Array();
        var sixdate = new Array();
        // months
        var firstmonth = new Array();
        var secondmonth = new Array();
        var thirdmonth = new Array();
        var fourthmonth = new Array();
        var fivemonth = new Array();
        var sixmonth = new Array();
        // year
        var firstyear = new Array();
        var secondyear = new Array();
        var thirdyear = new Array();
        var fourthyear = new Array();
        var fiveyear = new Array();
        var sixyear = new Array();
        for (var item of diasbledatearray.data.dates) {
            var datearray = (item.toString()).split('/');
            var formateddate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
            const d = new Date(formateddate);
            if (d.getMonth() == currentmonth) {
                firstdate.push(d.getDate());
                firstmonth.push(d.getMonth() + 1);
                firstyear.push(d.getFullYear());
            } else if (d.getMonth() == (currentmonth + 1)) {
                seconddate.push(d.getDate());
                (d.getMonth() + 1) > 12 ? secondmonth.push((d.getMonth() + 1) - 12) : secondmonth.push(d.getMonth() + 1);
                (d.getMonth() + 1) > 12 ? secondyear.push(d.getFullYear() + 1) : secondyear.push(d.getFullYear());
            } else if (d.getMonth() == (currentmonth + 2)) {
                thirddate.push(d.getDate());
                (d.getMonth() + 2) > 12 ? thirdmonth.push((d.getMonth() + 1) - 12) : thirdmonth.push(d.getMonth() + 1);
                (d.getMonth() + 2) > 12 ? thirdyear.push(d.getFullYear() + 1) : thirdyear.push(d.getFullYear());
            } else if (d.getMonth() == currentmonth + 3) {
                fourthdate.push(d.getDate());
                (d.getMonth() + 3) > 12 ? fourthmonth.push((d.getMonth() + 1) - 12) : fourthmonth.push(d.getMonth() + 1);
                (d.getMonth() + 3) > 12 ? fourthyear.push(d.getFullYear() + 1) : fourthyear.push(d.getFullYear());
            } else if (d.getMonth() == currentmonth + 4) {
                fivedate.push(d.getDate());
                (d.getMonth() + 4) > 12 ? fiveyear.push((d.getMonth() + 1) - 12) : fiveyear.push(d.getMonth() + 1);
                (d.getMonth() + 4) > 12 ? fiveyear.push(d.getFullYear() + 1) : fiveyear.push(d.getFullYear());
            } else if (d.getMonth() == currentmonth + 5) {
                sixdate.push(d.getDate());
                (d.getMonth() + 5) > 12 ? sixyear.push((d.getMonth() + 1) - 12) : sixyear.push(d.getMonth() + 1);
                (d.getMonth() + 5) > 12 ? sixyear.push(d.getFullYear() + 1) : sixyear.push(d.getFullYear());
            }
        }
        var text = [];
        var i;
        const getdatecheck = new Date();
        if (((getdatecheck.getDate()).toString()).length == 1) {
            var day = '0' + getdatecheck.getDate();
        } else {
            var day = '' + getdatecheck.getDate();
        }
        if (((getdatecheck.getMonth() + 1).toString()).length == 1) {
            var month = '0' + (getdatecheck.getMonth() + 1);
        } else {
            var month = '' + (getdatecheck.getMonth() + 1);
        }
        if (((getdatecheck.getHours()).toString()).length == 1) {
            var hourly = '0' + getdatecheck.getHours();
        } else {
            var hourly = '' + getdatecheck.getHours();
        }
        if (((getdatecheck.getMinutes() + 1).toString()).length == 1) {
            var minite = '0' + (getdatecheck.getMinutes() + 1);
        } else {
            var minite = '' + (getdatecheck.getMinutes() + 1);
        }
        let date = day + '/' + month + '/' + getdatecheck.getFullYear();

        this.rest.getdate(date, this.property_id, hourly + ':' + minite).subscribe((data) => {
            this.getdate = data.data;
        });
        this.markDisabled1 = (date: NgbDate) => {
            const d = new Date(date.year, date.month - 1, date.day);
            var arryschedule = diasbledatearray.data.schedule;
            if (arryschedule[0]['is_available'] == 0) {
                var weekno0 = d.getDay() == 0;
            } else {
                var weekno0 = d.getDay() == -1;
            }
            if (arryschedule[1]['is_available'] == 0) {
                var weekno1 = d.getDay() == 1;
            } else {
                var weekno1 = d.getDay() == -1;
            }
            if (arryschedule[2]['is_available'] == 0) {
                var weekno2 = d.getDay() == 2;
            } else {
                var weekno2 = d.getDay() == -1;
            }
            if (arryschedule[3]['is_available'] == 0) {
                var weekno3 = d.getDay() == 3;
            } else {
                var weekno3 = d.getDay() == -1;
            }
            if (arryschedule[4]['is_available'] == 0) {
                var weekno4 = d.getDay() == 4;
            } else {
                var weekno4 = d.getDay() == -1;
            }
            if (arryschedule[5]['is_available'] == 0) {
                var weekno5 = d.getDay() == 5;
            } else {
                var weekno5 = d.getDay() == -1;
            }
            if (arryschedule[6]['is_available'] == 0) {
                var weekno6 = d.getDay() == 6;
            } else {
                var weekno6 = d.getDay() == -1;
            }

            return weekno0 || weekno1 || weekno2 || weekno3 || weekno4 || weekno5 || weekno6 || (firstdate.indexOf(date.day) >= 0 && (firstmonth).indexOf(date.month) >= 0 && (firstyear).indexOf(date.year) >= 0) ||
                (seconddate.indexOf(date.day) >= 0 && secondmonth.indexOf(date.month) >= 0 && secondyear.indexOf(date.year) >= 0);
            (thirddate.indexOf(date.day) >= 0 && (thirdmonth).indexOf(date.month) >= 0 && (thirdyear).indexOf(date.year) >= 0) ||
            (fourthdate.indexOf(date.day) >= 0 && (fourthmonth).indexOf(date.month) >= 0 && (fourthyear).indexOf(date.year) >= 0) ||
            (fivedate.indexOf(date.day) >= 0 && (fivemonth).indexOf(date.month) >= 0 && (fiveyear).indexOf(date.year) >= 0) ||
            (sixdate.indexOf(date.day) >= 0 && (sixmonth).indexOf(date.month) >= 0 && (sixyear).indexOf(date.year) >= 0);
        }
    }


}
