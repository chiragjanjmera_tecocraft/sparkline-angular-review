import { Component, OnInit , NgZone } from '@angular/core';
import { RestApiService } from "../../rest-api.service";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';  

@Component({
    selector: 'app-favorite-spaces',
    templateUrl: './favorite-spaces.component.html',
    styleUrls: ['./favorite-spaces.component.css']
})
export class FavoriteSpacesComponent implements OnInit {
    propertys: any;
    itemsCount: number;
    imgURL: any = "assets/image/default.jpg";
    public loading = false;
    name:string;

    constructor(private rest: RestApiService,private toastr: ToastrService,private router : Router,private zone: NgZone) {
        if (!this.rest.currentUser()) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.loading = true;
        this.rest.myfaveritespace(this.page).subscribe((data: any) => {
            this.loading = false;
            if (data.status) {
                this.propertys = data.data.data;
                this.itemsCount = this.propertys.length;
            }else {
                this.propertys = [];
                this.itemsCount = 0;
            }
            
        });
        this.rest.getUserById().subscribe((data: any) => {
            this.name = data.data.firstname;
            if (data.data.image != '')
                this.imgURL = 'http://sparklinespace.tecocraft.co.in/console' + data.data.image;
        });
    }
    page = 1;
    
    pageChanged(event) {
        if (event.event.itemsCount) {} else {
            this.page = event.pageNo;
            this.rest.myfaveritespace(this.page).subscribe((data: any) => {
                this.propertys = data.data.data;
            });
        }
    }

    togglelike(event,propery_id) {
        event.stopPropagation();
        this.loading = true;
        this.rest.togglelike({property_id:propery_id}).subscribe((data: any) => {
            if (data.status) {
                this.toastr.success("Property removed successfully", "Success!");
            }
            this.rest.myfaveritespace(this.page).subscribe((data: any) => {
                if (!data.status) {
                    this.propertys = [];
                }
                if (data.status) {
                    this.propertys = data.data.data;
                    this.itemsCount = this.propertys.length;
                }else {
                    this.itemsCount = 0;
                }
            });
            this.loading = false;
        });
    }

    logout(){
        this.rest.logout();
        this.zone.runOutsideAngular(() => {
            location.reload();
        });
    }
}