import { Component, OnInit ,NgZone ,ViewChild} from "@angular/core";
import { RestApiService } from "../../rest-api.service";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';  

@Component({
    selector: "app-my-profile",
    templateUrl: "./my-profile.component.html",
    styleUrls: ["./my-profile.component.css"]
})
export class MyProfileComponent implements OnInit {
    constructor(private rest: RestApiService,private toastr: ToastrService,private router : Router,private zone: NgZone) {
        if (!this.rest.currentUser()) {
            this.router.navigate(['/']);
        }
    }
    firstname: string;
    lastname: string;
    email: string;
    mobile_no: string;
    imgURL: any = "assets/image/default.jpg";
    old_password:any;
    password:any;
    password_confirmation:any;
    name:string;
    public loading = false;
    conpass:boolean = false;
    @ViewChild('myform') form;
    ngOnInit() {
        this.loading = true;
        this.rest.getUserById().subscribe((data: any) => {
            this.loading = false;
            this.firstname = data.data.firstname;
            this.name = data.data.firstname;
            this.lastname = data.data.lastname;
            this.email = data.data.email;
            this.mobile_no = data.data.mobile_no;
            if (data.data.image != '')
                this.imgURL = 'http://sparklinespace.tecocraft.co.in/console' + data.data.image;
        });
    }
    fileToUpload: File = null;

    profileForm(form) {
        form.value.access_type = 1; // 1 for web, 2 -for app
        form.value.country_code = +91;
        var formData = new FormData();
        if (this.fileToUpload !== null) {
            formData.append('image', this.fileToUpload);
        }
        formData.append('firstname', form.value.firstname);
        formData.append('lastname', form.value.lastname);
        formData.append('mobile_no', form.value.mobile_no);
        formData.append('country_code', '+91');
        formData.append('access_type', '1');
        this.loading = true;
        this.rest.userprofile(formData).subscribe((data: {}) => {
            this.loading = false;
            this.toastr.success("profile successfully updated", "Success!");
            this.rest.getUserById().subscribe((data: any) => {
                this.name = data.data.firstname;
            });
        });
    }

    omit_special_char(event)
    {   
       var k;  
       k = event.charCode;  //         k = event.keyCode;  (Both can be used)
       return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
    }

    public imagePath;

    preview(files, file: FileList) {
        if (files.length === 0)
            return;

        var mimeType = files[0].type;
        if (mimeType.match(/image\/*/) == null) {
            // this.message = "Only images are supported.";
            return;
        }
        this.fileToUpload = file.item(0);
        var reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
            this.imgURL = reader.result;
        }
    }

    resetPassword(form) {
        if (form.value.password == form.value.password_confirmation) {
            this.loading = true;
            this.rest.updatePassoword(form.value).subscribe((data: any) => {
                this.loading = false;
                this.form.resetForm();
                if (data.status) {
                    this.old_password = '';
                    this.password = '';
                    this.password_confirmation = '';
                    this.toastr.success("password successfully updated", "Success!");
                } else {
                    this.toastr.error("old password field wrong", "error!");
                }
            });
        } else {
            this.conpass = true;
            setTimeout(()=>{
                this.conpass = false;
            }, 3000);
        }
    }

    logout(){
        this.rest.logout();
        this.zone.runOutsideAngular(() => {
            location.reload();
        });
    }
}