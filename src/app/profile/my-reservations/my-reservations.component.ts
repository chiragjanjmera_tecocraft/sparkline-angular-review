import { Component, OnInit , NgZone } from "@angular/core";
import { RestApiService } from "../../rest-api.service";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';  

@Component({
    selector: "app-my-reservations",
    templateUrl: "./my-reservations.component.html",
    styleUrls: ["./my-reservations.component.css"]
})
export class MyReservationsComponent implements OnInit {
    type: number = 1;
    propertys: any;
    itemsCount: any;
    page = 1;
    currentRate = 0;
    imgURL: any = "assets/image/default.jpg";
    name:string;

    public loading = false;
    constructor(private rest: RestApiService,private router : Router,private zone: NgZone,private toastr: ToastrService) {
        if (!this.rest.currentUser()) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.getproperty();
    }

    getproperty() {
        let form = {
            type: this.type,
            // property_id: 1,
            page: this.page
        };
        this.loading = true;
        this.rest.bookinghistory(form).subscribe((data: any) => {
            this.loading = false;
            if (data) {
                this.propertys = data.data.data;
                this.itemsCount = this.propertys.length;
            }
        });
        this.rest.getUserById().subscribe((data: any) => {
            this.name = data.data.firstname;
            if (data.data.image != '')
                this.imgURL = 'http://sparklinespace.tecocraft.co.in/console' + data.data.image;
        });
    }

    onChange(value) {
        this.type = value;
        this.getproperty();
    }

    pageChanged(event, signupForm) {
        if (event.event.itemsCount) {
        } else {
            this.page = event.pageNo;
            this.getproperty();
        }
    }

    cancelbooking(property_id){
        this.rest.cancelbooking(property_id).subscribe((data: any) => {
            if (data.status) {
                this.toastr.success("successfully canceled", "Success!");
                this.getproperty();
            } else {
                this.toastr.error('You can not cancel only before 24 hours!', "error!");
            }
        });
    }

    reviewproperty(form){
        if (form.value.rating) {
            this.rest.feedback(form.value).subscribe((data: any) => {
                if (data) {
                    this.toastr.success("successfully reviewed", "Success!");
                    this.getproperty();
                } else {
                    this.toastr.error("something went wrong", "error!");
                }
            });    
        } else {
            this.toastr.warning("Please give rating first", "warning!");
        }
    }

    logout(){
        this.rest.logout();
        this.zone.runOutsideAngular(() => {
            location.reload();
        });
    }
    
}
