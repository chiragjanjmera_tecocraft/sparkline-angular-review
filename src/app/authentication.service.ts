import { Injectable } from '@angular/core';
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Observable,of as observableOf ,Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {


	myMethod$: Observable<any>;
    private myMethodSubject = new Subject<any>();

    IsopenModel$: Observable<any>;
    private IsopenModelSubject = new Subject<any>();

    constructor() {
        this.myMethod$ = this.myMethodSubject.asObservable();
        this.IsopenModel$ = this.IsopenModelSubject.asObservable();
    }

    myMethod(data) { // set one class for dashboard page // for design 
        // I have data! Let's return it so subscribers can use it!
        // we can do stuff with data if we want
        this.myMethodSubject.next(data);
    }

    IsopenModel(data) { 
        this.IsopenModelSubject.next(data);
    }
}
