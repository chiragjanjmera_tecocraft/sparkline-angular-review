import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { SiteLayoutComponent } from "./_layout/site-layout/site-layout.component";
import { SiteHeaderComponent } from "./_layout/site-header/site-header.component";
import { SiteFooterComponent } from "./_layout/site-footer/site-footer.component";
import { FindSpaceComponent } from "./find-space/find-space.component";
import { BlogComponent } from "./blog/blog.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { AuthenticationService } from "./authentication.service";
import { HttpClientModule } from "@angular/common/http";
import { Ng5SliderModule } from "ng5-slider";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AgmCoreModule } from "@agm/core";
import { NgxPaginationModule } from 'ngx-pagination';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap';
import { PaginationModule } from 'ngx-pagination-bootstrap';
import { PropertyDetailComponent } from './property-detail/property-detail.component'
import { NgxGalleryModule } from 'ngx-gallery';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BookingComponent } from './booking/booking.component';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import { MyProfileComponent } from './profile/my-profile/my-profile.component';
import { FavoriteSpacesComponent } from './profile/favorite-spaces/favorite-spaces.component';
import { MyReservationsComponent } from './profile/my-reservations/my-reservations.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { NgxLoadingModule } from 'ngx-loading';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';    
import { ToastrModule } from 'ngx-toastr';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';  
@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        SiteLayoutComponent,
        SiteHeaderComponent,
        SiteFooterComponent,
        FindSpaceComponent,
        BlogComponent,
        ContactUsComponent,
        PropertyDetailComponent,
        BookingComponent,
        MyProfileComponent,
        FavoriteSpacesComponent,
        MyReservationsComponent,
        PrivacyPolicyComponent,
        TermsConditionComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        Ng5SliderModule,
        FormsModule,
        ReactiveFormsModule,
        AgmCoreModule.forRoot({
            apiKey: "AIzaSyAh9MadIQRuzP3SqMdcUpKsH08iGa4B0bg"
        }),
        NgxPaginationModule,
        PaginationModule,
        NgxGalleryModule,
        AgmSnazzyInfoWindowModule,
        NgMultiSelectDropDownModule.forRoot(),
        NgbModule,
        ModalModule.forRoot(),
        CreditCardDirectivesModule,
        SweetAlert2Module.forRoot(),
        NgxLoadingModule.forRoot({
            fullScreenBackdrop : true  
        }),
        BrowserAnimationsModule,  
        ToastrModule.forRoot(),
    ],
    providers: [AuthenticationService],
    bootstrap: [AppComponent]
})
export class AppModule {}
