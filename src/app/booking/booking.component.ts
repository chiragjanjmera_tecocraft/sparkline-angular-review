import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from "../authentication.service";
import { RestApiService } from "../rest-api.service";
import { CreditCardValidator } from 'angular-cc-library';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
@Component({
    selector: 'app-booking',
    templateUrl: './booking.component.html',
    styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

	property_id:any;
	property_data:any;
	imagePath:string = "public/images/property/space-1/1554885118.jpg";
	bookingdate:any;
	duration:any;
	withVat:any;
	vatPercentage:any;
	percenatgevalue:any;
    model: any = {};
    Settype:number;
    bookingData:any
    isError:boolean = false;
    Errormessage:String;
    public loading = false;
    constructor(private authenticationService: AuthenticationService, private toastr: ToastrService, private rest: RestApiService
        ,private router: Router) {

        let data = this.rest.getBookingddata(); // o = hourly  1 =  daily
        if (data[0].type == 0) {
        	var validary:any = data[0];
            this.bookingData = data[0];
            this.bookingdate = this.bookingData.date;
        } else {
        	var validary:any = data[1];
        	var firstDat:any = this.setdateformat(data[1].strdate);
            if (data[1].enddate) {
            	var ScnDat = this.setdateformat(data[1].enddate);
            }
        	this.bookingdate = firstDat;
        	if (ScnDat) {
				this.bookingdate = this.bookingdate + ' - ' + ScnDat;
        	}
            this.bookingData = data[1];
        }
        this.property_id = this.bookingData.property_id;
        this.Settype = validary.type;
        this.duration = validary.price;
        this.withVat = validary.withVat;
        this.vatPercentage = validary.vatPercentage;
        this.percenatgevalue = validary.percenatgevalue;;
        this.rest.getOnChangeProperty({property_id : this.property_id}).subscribe((data) => {
            this.property_data = data.data.data[0];
            if (this.property_data.property_images[0]) {
                this.imagePath = this.property_data.property_images[0].image;
            }
        });
    }

    ngOnInit() {

    }

    setdateformat(event){
        if ((event.day).toString().length == 1) {
            var day = '0' + event.day;
        } else {
            var day = '' + event.day;
        }
        if (((event.month).toString()).length == 1) {
              var month = '0' + event.month;
        } else {
              var month = '' + event.month;
        }
        let datefrm  = day + '/' + month + '/' + event.year;
        return datefrm;
    }

    payment() {
        var formData = new FormData();
        formData.append('card_no', (this.model.ccnumber).replace(/ /g,''));
        formData.append('cvv', this.model.cvcnumber);
        var expirydate = this.model.ccdate.split("/");
        formData.append('expiry_month', expirydate[0].trim());
        formData.append('expiry_year', expirydate[1].trim());
        formData.append('property_id', this.property_id);

        if (this.Settype == 0) {
            var type = '0'; // hourly
            formData.append('booking_date', this.bookingData.date);
            formData.append('start_time', this.bookingData.strTime);
            formData.append('end_time', this.bookingData.endTime);
            formData.append('type', type);
        } else {
            var type = '1'; // daily
            formData.append('type', type);
            if (this.bookingData.enddate) {
                formData.append('booking_date', this.setdateformat(this.bookingData.strdate));
                formData.append('end_date', this.setdateformat(this.bookingData.enddate));
            } else {
                formData.append('booking_date', this.setdateformat(this.bookingData.strdate));
                formData.append('end_date', this.setdateformat(this.bookingData.strdate));
            }
        }
        this.loading = true;
        this.rest.payment(formData,this.property_id).subscribe((data:any) => {
            this.loading = false;
            if (data.status) {
                this.toastr.success("property has been booked successfully", "Success!");
                this.router.navigate(['/my-reservations']);
            } else {
                this.isError = true;
                if (data.message) {
                    this.Errormessage = data.message;
                }
            }
        });
    }

}
