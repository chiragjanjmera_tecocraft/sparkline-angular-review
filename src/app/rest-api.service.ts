import { Injectable } from "@angular/core";
import {
    HttpClient,
    HttpHeaders,
    HttpErrorResponse
} from "@angular/common/http";
import { Observable, of } from "rxjs";
import { map, catchError, tap } from "rxjs/operators";
import { AuthenticationService } from './authentication.service';

@Injectable({
    providedIn: 'root'
})
export class RestApiService {
    
    apiURL: string = '';
    token:any;

    constructor(private httpClient: HttpClient,private authenticationService: AuthenticationService) {
        this.setToken();
    }

    /* get config data  */

    getconfiguration(): Observable < any > {
        return this.httpClient.get(this.apiURL + '/getconfiguration').pipe(
            map(this.extractData));
    }

    /* Add contanct us form data  */

    contactForm(form): Observable < any > { // JSON.stringify(form)
        return this.httpClient.post < any > (this.apiURL + '/contactUs', form,).pipe(
            tap((form) => ),
            catchError(this.handleError < any > ('contactForm'))
        );
    }

    /* get amenities list */

    getamenities() {
        return this.httpClient.post < any > (this.apiURL + '/amenities', {}).pipe(
            tap((form) => ),
            catchError(this.handleError < any > ('getamenities'))
        );
    }

    /* get categories list */

    getcategories() {
        return this.httpClient.post < any > (this.apiURL + '/categories', {}).pipe(
            tap((form) => ),
            catchError(this.handleError < any > ('getcategories'))
        );
    }

    /*  get current user    */

    currentUser() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }

    setToken(){
        if (this.currentUser()) {
            this.token = this.currentUser().web_token;
        } else {
            this.token = '';
        }
    }

    /* logout user destoy session */
    logout() {
        localStorage.removeItem('currentUser');
        this.setToken();
    }

    /*   user registration  */

    userRegistration(form): Observable < any > { // JSON.stringify(product)
        return this.httpClient.post < any > (this.apiURL + '/registration', form).pipe(
            tap((user) => ),
            catchError(this.handleError < any > ('addProduct'))
        );
    }

    /* user login form authentication */

    userLogin(form): Observable < any > { // JSON.stringify(product)
        form.access_type = 1
        return this.httpClient.post < any > (this.apiURL + '/login', form).pipe(
            tap((user) => {
                    if (user.status == true) {
                        localStorage.setItem('currentUser', JSON.stringify(user.data));
                        this.setToken();
                    }
                }
            ),
            catchError(this.handleError < any > ('addProduct'))
        );
    }

    /* forget password */

    forgetPassword(form): Observable < any > { // JSON.stringify(product)
        return this.httpClient.post < any > (this.apiURL + '/forgotpassword', form).pipe(
            tap((user) => {
                console.log(user)
            }),
            catchError(this.handleError < any > ('addProduct'))
        );
    }

    /* get property list  */

    getproperty(form) {
        if (this.currentUser()) {
            var user_id = this.currentUser().id;
            if (user_id) {
                form.user_id = user_id;
                form.access_type = 1;
            }
        }
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post < any > (this.apiURL + '/properties', form,httpOptions).pipe(
            tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('getproperty'))
        );
    }

    /* get property list by filter */

    getOnChangeProperty(form) {
        if (this.currentUser()) {
            var user_id = this.currentUser().id;
            if (user_id) {
                form.user_id = user_id;
                form.access_type = 1;
            }
        }
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post < any > (this.apiURL + '/properties', form,httpOptions).pipe(
            tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('getproperty'))
        );
    }

    /* subscibe news latter  */

    subscribeNewsLatter(form) {
        return this.httpClient.post < any > (this.apiURL + '/subscribe-newsletter', form).pipe(
            tap(),
            catchError(this.handleError < any > ('getproperty'))
        );
    }

    getdate(from, id, start_at) {
        return this.httpClient.post <any>(this.apiURL + '/getschedule', {
                'date': from,
                'property_id': id,
                'start_at': start_at
            })
            .pipe(
            tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('getdate'))
        );
    }

    getdiabledate(start_date, property_id, type) {
        return this.httpClient.post(this.apiURL + `/getdisableddates`, {
                'start_date': start_date,
                'property_id': property_id,
                'type': type
            }) // 0 Hourly, 1 Daily
            .pipe(map(this.extractData),
                catchError(this.handleError < any > ('getdiabledate'))
            );
    }

    checkavalablity(property_id, start_date, end_date, type) {
        return this.httpClient.post < any > (this.apiURL + `/propertyavailablity`, {
                property_id: property_id,
                start_date: start_date,
                end_date: end_date,
                type: type
            })
            .pipe(map(this.extractData),
                catchError(this.handleError < any > ('checkavalablity'))
            );
    }

    /* booking data for redirect property detail to booking page */
    setbookingData(bookingData){
        if (bookingData) {
            localStorage.setItem('bookingData',JSON.stringify(bookingData));
        }
    }

    getBookingddata(){
        return JSON.parse(localStorage.getItem('bookingData'));
    }

    payment(form,property_id) {
        var user_id = this.currentUser().id;
        form.append('user_id', user_id);
        form.append('access_type', 1);
        form.append('property_id', Number(property_id));
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post<any>(this.apiURL + `/booking`, form, httpOptions)
            .pipe(tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('payment'))
        );
    }

    userprofile(form) {
        form.append('user_id', this.currentUser().id);
        form.append('access_type', 1);
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post<any>(this.apiURL + `/userprofile`, form, httpOptions)
            .pipe(tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('userprofile'))
        );
    }

    getUserById() {
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post<any>(this.apiURL + `/getuserprofile`, {user_id : this.currentUser().id ,
            access_type : 1}, httpOptions)
            .pipe(tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('userprofile'))
        );
    }

    /*  change password  */
    updatePassoword(form) {
        form.user_id = this.currentUser().id ;
        form.access_type = 1 ;
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post<any>(this.apiURL + `/changePassword`, form, httpOptions)
            .pipe(tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('userprofile'))
        );
    }

    /*  get user favorite spaces  */
    myfaveritespace(page) {
        console.log(this.currentUser())
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post<any>(this.apiURL + `/myfaveritespace`, {'user_id' : this.currentUser().id ,
            'access_type' : 1 , 'page' : page}, httpOptions)
            .pipe(tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('userprofile'))
        );
    }

    /*  get booking history  */
    bookinghistory(form) {
        form.user_id     = this.currentUser().id;
        form.access_type = 1;
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post<any>(this.apiURL + `/booking-history`, form, httpOptions)
            .pipe(tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError <any> ('userprofile'))
        );
    }

    /*  cancel booking  */
    cancelbooking(property_id) {
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post<any>(this.apiURL + `/cancel-booking`, {'user_id' : this.currentUser().id ,
            'access_type' : 1 , 'booking_id' : property_id}, httpOptions)
            .pipe(tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('userprofile'))
        );
    }

    /*  feedback booking  */
    feedback(form) {
        form.user_id     = this.currentUser().id;
        form.access_type = 1;
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post<any>(this.apiURL + `/feedback`, form, httpOptions)
            .pipe(tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('feedback'))
        );
    }

    /*  toggle like  */
    togglelike(form) {
        form.user_id     = this.currentUser().id;
        form.access_type = 1;
        const httpOptions = {
            headers: new HttpHeaders({ //ea3ecsTXd15GJXNoQulwLoYkEWZrTn
                'auth-token': this.token,
            })
        };
        return this.httpClient.post<any>(this.apiURL + `/faveritespace`, form, httpOptions)
            .pipe(tap((user) => {
                if (user.status == false && user.message == "Invalid authentication token") {
                    this.logout();
                    this.authenticationService.IsopenModel("redirect to login page");
                    window.location.reload();
                }
            }),
            catchError(this.handleError < any > ('togglelike'))
        );
    }

    homedata() {
        return this.httpClient.post<any>(this.apiURL + `/homedata`,{},)
            .pipe(tap((user) => {}),
            catchError(this.handleError < any > ('userprofile'))
        );
    }

    getTermCondi(){
        return this.httpClient.get(this.apiURL + '/terms-condition').pipe(
            map(this.extractData));
    }

    getPrivacyPolicy(){
        return this.httpClient.get(this.apiURL + '/privacy-policy').pipe(
            map(this.extractData));
    }
    
    private extractData(res: Response) {
        // console.log(res);
        let body = res;
        return body || {};
    }

    /* handle an error */

    private handleError < T > (operation = 'operation', result ? : T) {
        return (error: any): Observable < T > => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
