import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { SiteLayoutComponent } from "./_layout/site-layout/site-layout.component";
import { SiteHeaderComponent } from "./_layout/site-header/site-header.component";
import { SiteFooterComponent } from "./_layout/site-footer/site-footer.component";
import { FindSpaceComponent } from "./find-space/find-space.component";
import { BlogComponent } from "./blog/blog.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { PropertyDetailComponent } from "./property-detail/property-detail.component";
import { BookingComponent } from "./booking/booking.component";
import { MyProfileComponent } from './profile/my-profile/my-profile.component';
import { FavoriteSpacesComponent } from './profile/favorite-spaces/favorite-spaces.component';
import { MyReservationsComponent } from './profile/my-reservations/my-reservations.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';  

const routes: Routes = [
    {
        path: "",
        component: SiteLayoutComponent,
        children: [
            {
                path: "",
                component: DashboardComponent
            },
            {
                path: "find-space",
                component: FindSpaceComponent
            },
            {
                path: "blog",
                component: BlogComponent
            },
            {
                path: "contact-us",
                component: ContactUsComponent
            },
            {
                path: "property-detail/:id",
                component: PropertyDetailComponent
            },
            {
                path: "booking",
                component: BookingComponent
            },
            {
                path: "myprofile",
                component: MyProfileComponent
            },
            {
                path: "favorite-space",
                component: FavoriteSpacesComponent
            },
            {
                path: "my-reservations",
                component: MyReservationsComponent
            },
            {
                path: "Privacy-Policy",
                component: PrivacyPolicyComponent
            },
            {
                path: "Terms-Condition",
                component: TermsConditionComponent
            },
        ]
    },
    // { path: '**', component: DashboardComponent}, // for wrong url redirect to not fould page
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
