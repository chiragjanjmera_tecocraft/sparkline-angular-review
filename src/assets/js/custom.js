
$(function () {
  "use strict";
  var Engine = {
    ui: {
      fastclick: function () {
        FastClick.attach(document.body);
        }, // end fastclick

        placeholder: function () {
          $('input, textarea').placeholder();
        }, // end placeholder

        parallax: function () {
          $('.parallax-window').jarallax({
            speed: 0.5, 
            imgWidth: 1366, 
            imgHeight: 768
          })

        }, // end parallax

        BGfillBox: function () {
          $('.fill-box').fillBox();
        }, // end BG fill Box

        RangeSlider: function () {
          $(".rangePrice").ionRangeSlider({
            hide_min_max: true, 
            keyboard: true, 
            min: 0, 
            max: 5500, 
            from: 0, 
            to: 7689256, 
            type: 'double', 
            step: 1, 
            prefix: "$", 
            grid: true
          });
          
          
        }, // end RangeSlider
        counterUp: function() {
          $('.counter').counterUp({
            delay: 10,
            time: 1000
          });
        },
        stickyHeader: function() {
          
          
          $(window).scroll(function() {
            if ($(this).scrollTop() >= 1) {
              $('.primary_nav').addClass('stickyHeader');
              $('.detail_breadcrumb').addClass('stickymove');
            }
            else {
              $('.primary_nav').removeClass('stickyHeader');
              $('.detail_breadcrumb').removeClass('stickymove');
            }
          });
          
          
        },

        misc: function () {
          $(".navbar-expand-toggle,.close-nav").on('click', function () {
            $(".app-container").toggleClass("expanded");
            return false;
          });
          $("#shoppingcart,.cart-close").on('click', function () {
            $("body").toggleClass("CartOpen");
            return false;
          });
          $(".btn-open-search,.btn-close-search").on('click', function () {
            $(".primary_nav").toggleClass("opensearch");
            return false;
          });
          

          var SideContentHeight = $('.block-cart-header').innerHeight() + + $('.block-cart-footer').innerHeight();
          $('.cart-middle-items').css('height',$( window ).height() - SideContentHeight + 'px')
          $('.navbar-expand-toggle').on('click', function() {
            $(this).toggleClass('open');

          });
          


        }, // end misc
        metisNavMenu: function () {
          $("#side-menu,#SBC_Nav").metisMenu();
        }, // end metisMenu

    }, // end ui
    
  };

  Engine.ui.fastclick();
  Engine.ui.placeholder();

  Engine.ui.parallax();
  Engine.ui.BGfillBox();

  Engine.ui.misc();
  Engine.ui.metisNavMenu();
  Engine.ui.RangeSlider();
  Engine.ui.counterUp();
  Engine.ui.stickyHeader();
});




$('.need-slider').slick({
  dots: false,
  arrows:true,
  infinite: true,
  autoplay:true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
  {
    breakpoint: 1025,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      dots: false,
      arrows:true
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 575,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  });

/* Bottom to top scroll*/
$(window).scroll(function() {
  if ($(this).scrollTop() > 50 ) {
    $('.scrolltop:hidden').stop(true, true).fadeIn();
  } else {
    $('.scrolltop').stop(true, true).fadeOut();
  }
});
$(function(){$(".scroll").click(function(){$("html,body").animate({scrollTop:$(".thetop").offset().top},"1000");return false})})

/* Bottom to top scroll*/


/*date picker*/
$('.input-group.date').datepicker({format: "dd.mm.yyyy"});
/*date picker*/


/*photo gallery light box*/
$(document).ready(function() {
  $(".fancybox-thumb").fancybox({
    prevEffect  : 'none',
    nextEffect  : 'none',
    helpers : {
      title : {
        type: 'outside'
      },
      thumbs  : {
        width : 70,
        height  : 70
      }
    }
  });
});
/*photo gallery light box*/

/* Sticky proprty tab Start */   
$(document).scroll(function(){
  var st = $(this).scrollTop();
  if(st >300) {
    $(".property_menu_wrapper_hidden").addClass('fixed');
    $(".property_menu_wrapper_hidden").addClass('slow');
  }
  else {
    $('.property_menu_wrapper_hidden').removeClass('fixed');;
  }
  
});

/* Sticky proprty tab End */

/* smooth scroling start */
$('.property_menu_item').on('click', function(e){
  $('html,body').animate({
    scrollTop: $($(this).attr('href')).offset().top - 100
  },500);
  e.preventDefault();
});

/* smooth scroling End */



//calendar property detial page start
$(document).ready(function() {
  
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,basicWeek,basicDay'
    },
    defaultDate: '2016-12-12',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
      {
        title: 'All Day Event',
        start: '2016-12-01'
      },
      {
        title: 'Long Event',
        start: '2016-12-07',
        end: '2016-12-10'
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: '2016-12-09T16:00:00'
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: '2016-12-16T16:00:00'
      },
      {
        title: 'Conference',
        start: '2016-12-11',
        end: '2016-12-13'
      },
      {
        title: 'Meeting',
        start: '2016-12-12T10:30:00',
        end: '2016-12-12T12:30:00'
      },
      {
        title: 'Lunch',
        start: '2016-12-12T12:00:00'
      },
      {
        title: 'Meeting',
        start: '2016-12-12T14:30:00'
      },
      {
        title: 'Happy Hour',
        start: '2016-12-12T17:30:00'
      },
      {
        title: 'Dinner',
        start: '2016-12-12T20:00:00'
      },
      {
        title: 'Birthday Party',
        start: '2016-12-13T07:00:00'
      },
      {
        title: 'Click for Google',
        url: 'https://google.com/',
        start: '2016-12-28'
      }
      ]
    });
  
});
//calendar property detial page end

//Login signup form start
$("#login1").click(function () {
  $("#logindiv").css("display","none")
  $("#regi1").css("display","block")
});

$("#signin1").click(function () {
  $("#regi1").css("display","none")
  $("#logindiv").css("display","block")
});

$("#reset1").click(function () {
  $("#logindiv").css("display","none")
  $("#forgotdiv").css("display","block")
});

$("#relog1").click(function () {
  $("#forgotdiv").css("display","none")
  $("#logindiv").css("display","block")
});

$("#widget_register_sw").click(function () {
  $("#login-div").css("display","none")
  $("#register-div").css("display","block")
});
$("#widget_login_sw").click(function () {
  $("#register-div").css("display","none")
  $("#login-div").css("display","block")
});
$("#forgot_pass_widget").click(function () {
  $("#login-div").css("display","none")
  $("#forgot-pass-div_shortcode").css("display","block")
});
$("#return_login_shortcode").click(function () {
  $("#forgot-pass-div_shortcode").css("display","none")
  $("#login-div").css("display","block")
});
//Login signup form end    

//Reduis miles slider
jQuery(document).ready(function(){
  $("#radius-slider").slider({
    value:5,
    min:1,
    max:50,
    step:1,
    slide:function(event, ui){
      $("#amount").val(ui.value)
    }
  });
  $("#amount").val(+$("#radius-slider").slider("value"));
});
//Reduis miles slider

window.onclick = function(event) {
  if (event.target.className == 'modal fade bs-modal-sm log-sign') {
    $('.modal')
    .find("input,textarea,select")
    .val('')
    .end()
    .find("input[type=checkbox], input[type=radio]")
    .prop("checked", "")
    .end();  
  }
}

$(document).ready(function () {
  $("#ToggleSideMenu").click(function (e) {
    e.preventDefault();
    $("#user_tab_menu_container").toggleClass("toggled");
  });
});
